'use strict';

var util = require('util');
var JSONStream = require('JSONStream');
var corsify = require('corsify')({ "Access-Control-Allow-Methods": "GET" });
var pump = require('pump');
var json2mongo = require('json2mongo');
var mongojs = require('mongojs');
var app = require('root')();

var objectId = /^[a-f\d]{24}$/i;
var number = /^\d+$/

var db;

var objectify = function (json) {
  if ('$oid' in json)
    return db.ObjectId(json['$oid']);
  Object.keys(json).forEach(function (key) {
    var value = json[key];
    if (typeof value === 'object')
      json[key] = objectify(value);
  });
  return json;
};

var parseJSON = function (json) {
  try {
    return JSON.parse(json);
  } catch (e) {
    return {};
  }
};

var getCursor = function (collection, options) {
  var query  = json2mongo(parseJSON(options.q)),
      filter = parseJSON(options.filter),
      sort   = parseJSON(options.sort),
      limit  = parseInt(options.limit || 0, 10),
      skip   = parseInt(options.skip  || 0, 10),
      collection = db.collection(collection),
      cursor;

  query = objectify(query);
  console.log('querying: ' + util.inspect(query, false, null));
  cursor = collection.find(query, filter).sort(sort).skip(skip);
  if (limit) cursor.limit(limit);
  return cursor;
};

var query = function (req, res) {
  console.log(req.method, req.url);
  var cursor = getCursor(req.params.collection, req.query);
  res.setHeader('Content-Type', 'application/json; charset=utf-8');
  pump(cursor, JSONStream.stringify(), res);
};

var get = function (req, res) {
  console.log(req.method, req.url);

  var id = req.params.id;
  if (objectId.test(id)) id = db.ObjectId(id);
  else if (number.test(id)) id = parseInt(id, 10);

  db.collection(req.params.collection).findOne({ _id: id }, function (err, doc) {
    if (err) {
      res.statusCode = 500;
      res.setHeader('Content-Type', 'text/plain');
      res.end('Error: ' + err.message);
      return;
    }
    if (!doc) {
      res.writeHead(404);
      res.end();
      return;
    }
    res.setHeader('Content-Type', 'application/json; charset=utf-8');
    res.end(JSON.stringify(doc));
  });
};

//Custom service to return accumulated kWH, min and max dates
var sum = function (req, res) {
  console.log(req.method, req.url);
  var returnDoc;

  //Get accumulated Watts sum
  db.collection(req.params.collection).aggregate([{$group:{_id:null,AccumulatedWatts:{$sum:"$Watts"}}}], function (err, doc) {
    if (err) {
      res.statusCode = 500;
      res.setHeader('Content-Type', 'text/plain');
      res.end('Error: ' + err.message);
      return;
    }
    if (!doc) {
     res.writeHead(404);
      res.end();
      return;
    }
    console.log(JSON.stringify(doc));

  //Get earliest datetime sensor reading
   db.collection(req.params.collection).aggregate({$group:{_id:null,StartDate:{$min:"$DateTime"}}}, function (err, mindoc) {
    if (err) {
      res.statusCode = 500;
      res.setHeader('Content-Type', 'text/plain');
      res.end('Error: ' + err.message);
      return;
    }
    if (!mindoc) {
     res.writeHead(404);
      res.end();
      return;
    }
    var minHours = new Date(mindoc[0].StartDate);
   console.log(JSON.stringify(mindoc));

   //Get letest datetime sensor reading (should be pretty close to now
   db.collection(req.params.collection).aggregate([{$group:{_id:null,EndDate:{$max:"$DateTime"}}}], function (err, maxdoc) {
    if (err) {
      res.statusCode = 500;
      res.setHeader('Content-Type', 'text/plain');
      res.end('Error: ' + err.message);
      return;
    }
    if (!maxdoc) {
     res.writeHead(404);
      res.end();
      return;
    }

    //calculated the number of hours between stant end end date.
    console.log(maxdoc[0].EndDate);
    var maxHours = new Date(maxdoc[0].EndDate);
    console.log(JSON.stringify(maxdoc));
    var diffHours = (maxHours-minHours)/(1000*60*60);    
    console.log("DiffHours:"+diffHours);

    //Combine into one return JSON
    doc[0].AccumulatedHours = diffHours;     
    doc[0].EarliestDate = mindoc[0].StartDate;
    doc[0].Latestdate = maxdoc[0].EndDate;

    console.log(JSON.stringify(doc));
    res.setHeader('Content-Type', 'application/json; charset=utf-8');
    res.end(JSON.stringify(doc));


      });  
    });
  });
};


module.exports = function (mongoUri) {
  db = mongojs(mongoUri);

  app.get('/{collection}', corsify(query));
  app.get('/{collection}/{id}', corsify(get));
  app.get('/{collection}/sum/{value}', corsify(sum));

  return app;
};
